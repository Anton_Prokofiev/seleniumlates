﻿using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapyFramework.Page;

namespace SeleniumUITestFramework.Controls
{
    interface IInit
    {
        void Init(RemoteWebDriver driver);
    }
}