﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls.LightBoxes;
using OpenQA.Selenium.Interactions;

namespace SeleniumUITestFramework.Controls
{
    static class IWebDriverExtentions
    {
        /// <summary>
        /// This method helps Initialize any page.
        /// </summary>
        /// <typeparam name="TPage"></typeparam>
        /// <param name="driver"></param>
        /// <param name="page"></param>
        /// <param name="pageInit"></param>
        /// <returns></returns>
        public static RemoteWebDriver InitPage<TPage>(this RemoteWebDriver driver, TPage page, Action<TPage> pageInit)
        {
            pageInit(page);
            return driver;
        }
        /// <summary>
        /// This method helps Create Instance of LightBox.
        /// </summary>
        /// <typeparam name="TBox"></typeparam>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static TBox CreateLightBox<TBox>(this RemoteWebDriver driver) where TBox : ILightBoxControl, new()
        {
            var lightBox = new TBox();
            lightBox.Init(driver);
            return lightBox;
        }
    }
}
