﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SeleniumUITestFramework.Controls.BreadCrumbs
{
    class BreadCrumbsList : IBreadCrumbsControl
    {
        private Lazy<IList<IWebElement>> BreadCrumbList;
        private RemoteWebDriver driver;
        public BreadCrumbsList(RemoteWebDriver driver)
        {
            this.driver = driver;
        }
        

        public void Init(RemoteWebDriver driver)
        {
            BreadCrumbList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector(".os-breadcrumbs-list>div"));
        }


        /*      public bool RetriveBreadCrumpSelectedValue(int index, string status)
              {
                  var breadcrumplist = BreadCrumbList.Value;
                  bool s = breadcrumplist.GetAttribute("class").Contains(status);
                  if (status == "active")
                     return s=true;
                  else if (status == "frozen")
                     return s=true;
                  else if (status == "uncompleted")
                     return s=true;
                  return s;
              }
      */
        private bool isValidBreadCrumbs(int breadCrumbIndex)
        {
            var listLenght = BreadCrumbList.Value.Count;
            var isValidBreadCrumbs = true;

            for (var index = 0; index < listLenght; index++)
            {
                if (!isValidBreadCrumbs) break;

                if (index < breadCrumbIndex - 1)
                {
                    isValidBreadCrumbs = BreadCrumbList.Value[index].GetAttribute("class").Contains("frozen");
                }
                else if (index == breadCrumbIndex - 1)
                {
                    isValidBreadCrumbs = BreadCrumbList.Value[index].GetAttribute("class").Contains("active");
                }
                else if (index > breadCrumbIndex - 1)
                {
                    isValidBreadCrumbs = BreadCrumbList.Value[index].GetAttribute("class").Contains("uncompleted");
                }
            }

            return isValidBreadCrumbs;
        }
        public bool waitForValidBreadcrumbs(int breadCrumbIndex,int waitSec = 5)
        {
            return new WebDriverWait(this.driver, TimeSpan.FromSeconds(waitSec)).Until(waitingForValidBreadcrumbs(breadCrumbIndex));
        }
        private Func<IWebDriver, bool> waitingForValidBreadcrumbs(int breadCrumbIndex)
        {
            return driver =>
            {
                return isValidBreadCrumbs(breadCrumbIndex);
            };
        }
    }
}
