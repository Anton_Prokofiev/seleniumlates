﻿using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumUITestFramework.Controls
{
    public static class Api
    {

        public static IRestResponse ApiCall(this RemoteWebDriver driver, String restClient, String restRequest, Method method)
        {
            Thread.Sleep(500);
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 8));
            wait.Until(wd => driver.Manage().Cookies.GetCookieNamed("access-token").Value != null);
            String accessToken = driver.Manage().Cookies.GetCookieNamed("access-token").Value;

            //! get info for car unblock api call

            IRestClient client = new RestClient(restClient);
            IRestRequest request = new RestRequest(restRequest, method);

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic d2F0b3N1c2VyOlNVMzkycGVy");
            request.AddHeader("x-authorization", "Bearer " + accessToken);


            return client.Execute(request);
        }
        /// <summary>
        /// Get status code response
        /// </summary>
        /// <param name="requestUrl"> url </param>
        /// <param name="method"> method = "GET"||"POST" ... </param>
        public static int GetStatusCodeResponse(String requestUrl, String method = "GET")
        {
            //fix for The request was aborted: Could not create SSL/TLS secure channel(for windows it works)
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            request.Method = method;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            HttpStatusCode status = response.StatusCode;
            int resposneStatusCode = (int)status;
            response.Close();
            return resposneStatusCode;
        }

    }
}
