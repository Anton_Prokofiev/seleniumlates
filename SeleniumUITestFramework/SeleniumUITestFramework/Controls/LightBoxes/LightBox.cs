﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using CapyFramework.Helpers;
using CapyFramework.Helpers.Waiter;

namespace SeleniumUITestFramework.Controls.LightBoxes
{
    class LightBox : ILightBoxControl
    {
        private RemoteWebDriver driver;

        private Lazy<IWebElement> DataPolicyAgreementChkBox;
        private Lazy<IWebElement> DataPolicyAgreementBtn;
        private Lazy<IWebElement> FirstNameTextField;
        private Lazy<IWebElement> SecondNameTextField;
        private Lazy<IWebElement> StreetTextField;
        private Lazy<IWebElement> HouseNumberTextField;
        private Lazy<IWebElement> ZipCodeTextField;
        private Lazy<IWebElement> CityTextField;
        private Lazy<IWebElement> SubmitBtn;
        private Lazy<IWebElement> CloseBtn;
        private Lazy<IWebElement> DigitalSellerPhone;
        private Lazy<IList<IWebElement>> TitleDropDownList;
        private Lazy<IList<IWebElement>> SalutationDropDownList;
        private Lazy<IWebElement> TitleDropDownbtn;
        private Lazy<IWebElement> SalutationDropDownbtn;
        private Lazy<IWebElement> PhoneField;

        private readonly By PaymentFinancingMethodConfirmBtnLocator = By.XPath("//button[@id='payment-method-c2a-confirm']");


        public void Init(RemoteWebDriver driver)
        {
            this.driver = driver;
            // TODO rewrite this lazy elements
            TitleDropDownbtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#title"));
            TitleDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector("#title > option"));
            SalutationDropDownbtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#salutation"));
            SalutationDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector("#salutation > option"));
            DataPolicyAgreementChkBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-checkbox__icon"));
            DataPolicyAgreementBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-button--primary"));
            FirstNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--firstName"));
            SecondNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--lastName"));
            StreetTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--street"));
            HouseNumberTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--houseNumber"));
            ZipCodeTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--zipCode"));
            CityTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--city"));
            SubmitBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-button--primary"));
            CloseBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-icon--extra-small"));


            DigitalSellerPhone = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-headline.os-headline--2"));
        }

        public string GetPhoneValue()
        {
            return DigitalSellerPhone.Value.Text;
        }
        public void SubmitDataPolicyAgreement()
        {
            DataPolicyAgreementChkBox.Value.Click();
            DataPolicyAgreementBtn.Value.Click();
        }

        public void CloseReservedCarDialog()
        {
            CloseBtn.Value.Click();
        }
        public void OpenDataProtectionPolicy()
        {
            driver.S(By.CssSelector(".os-checkbox__label > :nth-child(1)")).clickOnElement();
        }

        public void OpenTermsOfUse()
        {
            driver.S(By.CssSelector(".os-checkbox__label > :nth-child(2)")).clickOnElement();
        }


        public LightBox ClearBillingForm(RemoteWebDriver driver)
        {
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(Keys.Control + "a");
            FirstNameTextField.Value.SendKeys(Keys.Delete);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(Keys.Control + "a");
            SecondNameTextField.Value.SendKeys(Keys.Delete);
            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(Keys.Control + "a");
            StreetTextField.Value.SendKeys(Keys.Delete);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(Keys.Control + "a");
            HouseNumberTextField.Value.SendKeys(Keys.Delete);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(Keys.Control + "a");
            ZipCodeTextField.Value.SendKeys(Keys.Delete);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(Keys.Control + "a");
            CityTextField.Value.SendKeys(Keys.Delete);
            return driver.CreateLightBox<LightBox>();
        }
        public void FillInDeliveryForm(RemoteWebDriver driver)
        {
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingFirstName"]);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingSecondName"]);
            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingStreet"]);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingHouseNumber"]);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingZipCode"]);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingCity"]);
            //            bool isDisplayed = driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--mobile").Displayed;
            try
            {
                var b = driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--mobile").Displayed;
                PhoneField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--mobile"));
                PhoneField.Value.Click();
                PhoneField.Value.SendKeys(ConfigurationManager.AppSettings["PhoneNumber"]);
            }
            catch (NoSuchElementException e)
            {
                SubmitBtn.Value.Click();
                Console.WriteLine(e.Message);
            }
        }

        public void PKWFillInDeliveryForm(RemoteWebDriver driver)
        {



            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingFirstName"]);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingSecondName"]);
            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingStreet"]);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingHouseNumber"]);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingZipCode"]);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(ConfigurationManager.AppSettings["BillingCity"]);


            SalutationDropDownbtn.Value.Click();
            Lazy<IWebElement> SalutationOption = new Lazy<IWebElement>(() => driver.FindElementByXPath("//select[@id='salutation']/option[2]"));
            SalutationOption.Value.Click();

            Lazy<IWebElement> TitleOption = new Lazy<IWebElement>(() => driver.FindElementByXPath("//select[@name='title']/option[2]"));
            TitleOption.Value.Click();

            SubmitBtn.Value.Click();
        }
        /// <summary>
        /// Click on confirm Financing button for FSAG process
        /// </summary>
        /// <param name="withShift">true for redirect to Mock FSAG page</param>
        public void PKWPaimentMethodConfirm(RemoteWebDriver driver, Boolean withShift = true)
        {
            if (withShift)
            {
                driver.S(PaymentFinancingMethodConfirmBtnLocator).waitForElementClickable(driver).shiftClick(driver);
            }
            else
            {
                driver.S(PaymentFinancingMethodConfirmBtnLocator).clickOnElement();
            }
        }
    }
}
