﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CapyFramework.Page;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SeleniumUITestFramework.Controls
{
    public class WaitHelper : MainPage
    {
        public WebDriverWait wait;

        public WaitHelper(RemoteWebDriver driver) : base(driver)
        {
        }

        public void WaitHelperInit(RemoteWebDriver driver, bool IsPageLoaded)
        {
            if (wait == null)
                wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(d => IsPageLoaded);
        }

        public bool IsElementPresent(RemoteWebDriver driver, bool IsPageLoaded)
        {
            try
            {
                wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
                wait.Until(d => IsPageLoaded);

                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void WaitForReady(RemoteWebDriver driver, int waitSec = 10)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
            wait.Until(d =>
            {
                //Thread.Sleep(1500);
                try
                {
                    driver.FindElement(By.ClassName("os-loading-spinner"));
                    return false;
                }
                catch
                {
                    return true;
                }
            });
        }
    }
}
