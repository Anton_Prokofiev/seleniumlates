﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapyFramework.Helpers;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.PagesObjects;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects.PKW;
using System.Configuration;

namespace SeleniumUITestFramework.Tests.PKWRegressionTests
{
    [Category("Regression PKW")]
    class VerifyHomePageTest : PKWMainTest
    {
        [Test(Description =
                "Verify that home page has displayed 'Dealers info section'")]
        [TestCase("?key=REVVNDcwMDdWRFYyMTY=", "Chrome")]
        public void VerifyThatHomePageHasDisplayedDealersInfoSection(String carKey, string BrowserName)
        {
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new PKWHomePage(driver), homepage =>
            {
                homepage.Wait().WaitForReady(driver);

                Assert.That(homepage.IsDealerInfoFooterDisplayed, Is.True, "Dealer section on home page isn't displayed");
                Assert.That(homepage.IsDealerInfoTitleDisplayed, Is.True, "Isn't displayed TITLE data in dealer section on home page");
                Assert.That(homepage.IsDealerInfoAdressDisplayed, Is.True, "Isn't displayed ADRESS data in dealer section on home page");
                Assert.That(homepage.IsDealerInfoTelephoneDisplayed, Is.True, "Isn't displayed TELEPHONE data in dealer section on home page");
                Assert.That(homepage.IsDealerInfoEmailDisplayed, Is.True, "Isn't displayed EMAIL data in dealer section on home page");

                Assert.That(homepage.VerifyThatDealerFooterLinkIsDisplayedById(1), Is.True, "Isn't displayed 'AGB' link in dealer footer section on home page");
                Assert.That(homepage.VerifyThatDealerFooterLinkIsDisplayedById(2), Is.True, "Isn't displayed 'Datenschutzerklärung' in dealer footer section on home page");
                Assert.That(homepage.VerifyThatDealerFooterLinkIsDisplayedById(3), Is.True, "Isn't displayed 'Nutzungsbedingungen' in dealer footer section on home page");
                Assert.That(homepage.VerifyThatDealerFooterLinkIsDisplayedById(4), Is.True, "Isn't displayed 'Impressum' in dealer footer section on home page");

                Assert.That(homepage.VerifyThatDealerFooterLinkIsValidById(1), Is.True, "Isn't Valid(response code not 200) 'AGB' link in dealer footer section on home page");
                Assert.That(homepage.VerifyThatDealerFooterLinkIsValidById(2), Is.True, "Isn't Valid(response code not 200) 'Datenschutzerklärung' in dealer footer section on home page");
                Assert.That(homepage.VerifyThatDealerFooterLinkIsValidById(3), Is.True, "Isn't Valid(response code not 200) 'Nutzungsbedingungen' in dealer footer section on home page");
                Assert.That(homepage.VerifyThatDealerFooterLinkIsValidById(4), Is.True, "Isn't Valid(response code not 200) 'Impressum' in dealer footer section on home page");

                homepage.ClickOnFaqUrlInDealerInfoSection();
                String LastOpenedPageUrl = GetDriver().SwitchTo().Window(GetDriver().WindowHandles.Last()).Url;
                String PKWFaqPageUrl = getAppSettings("PKWFaqUrl");
                Assert.That(LastOpenedPageUrl, Is.EqualTo(PKWFaqPageUrl), "FAQ page isn't equal PKWFaqUrl = " + PKWFaqPageUrl);   
            });
        }
        [Test(Description =
                "Verify that home page has displayed valid links on privacy light box")]
        [TestCase("?key=REVVNDcwMDdWRFYyMTY=", "Chrome")]
        public void VerifyThatPrivacyLightBoxLinksIsValid(String carKey, string BrowserName)
        {
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new PKWHomePage(driver), homepage =>
            {
                homepage.Wait().WaitForReady(driver);
                homepage.OrderOnlineNowBtnClick();
                Assert.That(homepage.VerifyThatPrivacyLightBoxLiksIsValidById(1), Is.True, "Isn't Valid(response code not 200) 'Datenschutzerklärung' in privacy light box on home page");
                Assert.That(homepage.VerifyThatPrivacyLightBoxLiksIsValidById(2), Is.True, "Isn't Valid(response code not 200) 'Nutzungsbedingungen' in privacy light box on home page");
            });
        }
    }
}
