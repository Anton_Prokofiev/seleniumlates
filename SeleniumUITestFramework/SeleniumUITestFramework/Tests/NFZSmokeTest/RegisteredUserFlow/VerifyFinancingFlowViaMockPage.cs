﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapyFramework.Helpers;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.PagesObjects.NFZ;
using System.Threading;

namespace SeleniumUITestFramework.Tests.NFZSmokeTest.RegisteredUserFlow
{
    [Category("Smoke NFZ")]
    class VerifyFinancingFlowViaMockPage : NFZMainTest
    {
        [Test(Description = "Verify that user can successfully walk-through Financing flow via MockPage (already registered user)")]
        [TestCase("?key=REVVMDE5MzcxMzAyNTY=", "Chrome")]
        public void VerifyThatUserCanWalkThroughPrivatFinancingFlowRegisteredUserViaMock(String key, String BrowserName)
        {

            SetCarKey(key);
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
           .InitPage(new NFZHomePage(driver), homepage =>
           {
               homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
               homepage.FinancingMethodClick();
               homepage.OnlinePurchaseBtnClick(driver);
           })
           .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
           {
               loginpage.Login();
               int carUnblockResposeCode = UnlockCurrentCar();
               Console.WriteLine("car unblocked and ruturned status code = " + carUnblockResposeCode);
           })
           .InitPage(new NFZRegistrationPage(driver), registrationpage =>
           {
               registrationpage.ContinueOnlineOrderClick();
           })
           .InitPage(new NFZWelcomePage(driver), welcomepage =>
           {
               welcomepage.SubmitBtnClick();
           })
           .InitPage(new NFZContactDataPage(driver), contactDataPage =>
           {
               contactDataPage.SelectPhoneCheckBox(driver);
               contactDataPage.ClearContactFormWithPhoneField(driver);
               contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
               contactDataPage.FillInContactFormWithPhone(driver);
           })
           .InitPage(new NFZPaymentPage(driver), paymentPage =>
           {
               paymentPage.FinancingContinueBtnClick();
           })
            .InitPage(new MockFSAGPage(driver), mockPage =>
            {
                mockPage.IsFsagResponceBlockDisplayed();
                Thread.Sleep(500); // compelled wait for automatically fill all fields
                mockPage.ClickOnSendBtn();
            })
            .InitPage(new NFZFinancingApprovedPage(driver), page =>
            {
                page.ClickOnNextBtn();
            })
            .InitPage(new NFZPaymentPage(driver), paymentPage =>
            {
                String price = paymentPage.GetPriceValue(driver);
                paymentPage.SubmitPaymentBtnClick();
            })
           .InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
           {
               deliveryPage.ClearDeliveryForm();
               deliveryPage.FillInDeliveryForm(driver);
               deliveryPage.SubmitForm();
           })
           .InitPage(new NFZOrderOverviewPage(driver), orderOverviewPage =>
           {
               orderOverviewPage.SubmitDeliveryRemarks();
           }
           ).InitPage(new NZFOrderSuccessPage(driver), orderSuccessPage =>
           {
               Assert.That(orderSuccessPage.IsOrderSuccessSectionDisplayed, "Order success page isn't displayed");
               UnlockCurrentCar();
           }
           );
        }
    }
}
