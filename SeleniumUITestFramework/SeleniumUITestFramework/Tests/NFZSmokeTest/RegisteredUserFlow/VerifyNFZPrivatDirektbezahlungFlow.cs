﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumUITestFramework.PagesObjects.NFZ;
using OpenQA.Selenium.Remote;


namespace SeleniumUITestFramework.Tests
{
    [TestFixture]
    [Category("Smoke NFZ")]
    public class VerifyNFZPrivatDirektbezahlungFlow : NFZMainTest
    {

        [Test(Description = "Verify that user can successfully walk-through Privat/Direktbezahlung flow (already registered user)")]
        [TestCase("?key=REVVMDE5MzcyNTU3MTM%3D", "Chrome")]
        //[TestCase("?key=REVVMDE5MzcyMzYwNTk%3D", "Firefox")]
        public void VerifyThatUserCanWalkThroughPrivatDirektbezahlungFlowRegisteredUser(String carKey, String browserName)
        {
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp(browserName);
            driver
           .InitPage(new NFZHomePage(driver), homepage =>
           {
               homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
               homepage.DirectPaymentTileClick();
               homepage.OnlinePurchaseBtnClick(driver);
           })
           .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
           {
               loginpage.Login();
           })
           .InitPage(new NFZRegistrationPage(driver), registrationpage =>
           {
               registrationpage.ContinueOnlineOrderClick();
           })
           .InitPage(new NFZWelcomePage(driver), welcomepage =>
           {
               welcomepage.SubmitBtnClick();
           })
           .InitPage(new NFZContactDataPage(driver), contactDataPage =>
           {
               contactDataPage.SelectPhoneCheckBox(driver);
               contactDataPage.ClearContactFormWithPhoneField(driver);
               contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
               contactDataPage.FillInContactFormWithPhone(driver);
           })
           .InitPage(new NFZPaymentPage(driver), paymentPage =>
           {
               paymentPage.WaitForPaymentAmountExist();
               paymentPage.SubmitPayment();
           })
           .InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
           {
               deliveryPage.ClearDeliveryForm();
               deliveryPage.FillInDeliveryForm(driver);
               deliveryPage.SubmitForm();
           })
           .InitPage(new NFZOrderOverviewPage(driver), orderOverviewPage =>
           {
               orderOverviewPage.SubmitDeliveryRemarks();
           }
           ).InitPage(new NZFOrderSuccessPage(driver), orderSuccessPage =>
           {
               Assert.That(orderSuccessPage.IsOrderSuccessSectionDisplayed, "Order success page isn't displayed");
           });
        }
    }
}
