﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumUITestFramework.Controls.LightBoxes;
using SeleniumUITestFramework.PagesObjects.NFZ;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Tests
{
    [Category("Smoke NFZ")]
    class VerifyNFZGewerbeDirektbezahlungFlow : NFZMainTest
    {

        [Test(Description = "Verify that user can successfully walk-through Gewerbe/Direktbezahlung flow (already registered user)")]
        [TestCase("?key=REVVMDE5MzcyMzYwNTk%3D", "Chrome")]
        public void VerifyThatUserCanWalkThroughGewerbeDirektbezahlungFlowRegisteredUserNettoPrice(string CarKey, string BrowserName)
        {
            SetCarKey(CarKey);
            var price = default(string);
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new NFZHomePage(driver), homepage =>
            {
                homepage.Wait().WaitForReady(driver);
                //                    homepage.CookieNotificationClose();
                homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
                homepage.GewerbeTileClick();
                homepage.OnlinePurchaseBtnClick(driver);
            })
            .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
            {
                loginpage.Login();
            })
            .InitPage(new NFZRegistrationPage(driver), registrationpage =>
            {
                registrationpage.Wait().WaitForReady(driver);
                registrationpage.ContinueOnlineOrderClick();
            })
            .InitPage(new NFZWelcomePage(driver), welcomepage =>
            {
                welcomepage.Wait().WaitForReady(driver);
                welcomepage.SubmitBtnClick();
            })
            .InitPage(new NFZContactDataPage(driver), contactDataPage =>
            {
                contactDataPage.Wait().WaitForReady(driver);
                contactDataPage.SelectPhoneCheckBox(driver);
                contactDataPage.ClearContactFormWithPhoneField(driver);
                Assert.That(contactDataPage.IsFirstNameErrorMsgDisplayed, Is.True, "FirstName error message is not displayed");
                Assert.That(contactDataPage.FirstNameErrorMsg.Text, Is.EqualTo(Resources.FirstNameErrorMsg), "First name error message text is different");
                Assert.That(contactDataPage.IsLastNameErrorMsgDisplayed, Is.True, "LastName error message is not displayed");
                Assert.That(contactDataPage.LastNameErrorMsg.Text, Is.EqualTo(Resources.LastNameErrorMsg), "Last name error message text is different");
                Assert.That(contactDataPage.IsEmailErrorMsgDisplayed, Is.True, "Email error message is not displayed");
                Assert.That(contactDataPage.EmailErrorMsg.Text, Is.EqualTo(Resources.EmailErrorMsg), "Email error message text is different");
                Assert.That(contactDataPage.IsTelephoneErrorMsgDisplayed, Is.True, "Phone number error message is not displayed");
                Assert.That(contactDataPage.TelephoneErrorMsg.Text, Is.EqualTo(Resources.TelephoneNumberErrorMsg), "Telephone error message text is different");
                contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
                contactDataPage.FillInContactFormWithPhone(driver);
            })
            .InitPage(new NFZPaymentPage(driver), paymentPage =>
            {
                paymentPage.Wait().WaitForReady(driver);
                price = paymentPage.GetPriceValue(driver);
                paymentPage.SubmitPayment();
            })
            .InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
            {
                deliveryPage.Wait().WaitForReady(driver);
                deliveryPage.ClearDeliveryForm();
                deliveryPage.FillInDeliveryForm(driver);
                deliveryPage.OpenBillindAddressModal(driver).ClearBillingForm(driver).FillInDeliveryForm(driver);

                Thread.Sleep(2000);
                deliveryPage.SubmitForm();
            })
            .InitPage(new NFZOrderOverviewPage(driver), orderOverviewPage =>
            {
                orderOverviewPage.Wait().WaitForReady(driver);
                Assert.That(orderOverviewPage.RetrieveContactDataValues(3), Is.EqualTo(Resources.TelephoneNumber), "Telephone number is incorrect");
                Assert.That(orderOverviewPage.RetrieveContactDataValues(1), Is.EqualTo(Resources.Email), "Email address is incorrect");
                Assert.That(orderOverviewPage.RetrievePaymentDataValues(3), Is.EqualTo(price + Resources.NettoPrice), "Car price is not displayed");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.FirstName + Resources.SecondName),
                    "Salutation, Title, First name or Second name is not correct");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(1), Is.EqualTo(Resources.Street + Resources.HouseNumber),
                    "Street or House number is not correct");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(3), Is.EqualTo(Resources.ZipCode + Resources.City),
                    "ZipCode or City is not correct");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.BillingFirstName + Resources.BillingSecondName),
                    "Salutation, Title, First name or Second name is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(1), Is.EqualTo(Resources.BillingStreet + Resources.BillingHouseNumber),
                    "Street or House number is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(3), Is.EqualTo(Resources.BillingZipCode + Resources.BillingCity),
                    "ZipCode or City is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");

                orderOverviewPage.SubmitDeliveryRemarks();
                Thread.Sleep(1000);
            }).InitPage(new NFZConfirmationPage(driver), confirmationPage =>
                {
                    confirmationPage.Wait().WaitForReady(driver);
                    Assert.That(confirmationPage.RetrievePrice(), Is.EqualTo(price + Resources.NettoPrice),
                        "Car price is not displayed");
                });
        }

        [Test(Description = "Verify that user can successfully walk-through Gewerbe/Direktbezahlung flow (already registered user)")]
        [TestCase("?key=REVVMDE5MzcyNTU3MTM%3D", "Firefox")]
        public void VerifyThatUserCanWalkThroughGewerbeDirektbezahlungFlowRegisteredUserBruttoPrice(string carKey, string BrowserName)
        {
            
            var price = default(string);
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
                .InitPage(new NFZHomePage(driver), homepage =>
                {
                    homepage.Wait().WaitForReady(driver);
                    //                    homepage.CookieNotificationClose();
                    homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
                    homepage.GewerbeTileClick();
                    homepage.OnlinePurchaseBtnClick(driver);
                })
                .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
                {
                    loginpage.Login();
                })
                .InitPage(new NFZRegistrationPage(driver), registrationpage =>
                {
                    registrationpage.Wait().WaitForReady(driver);
                    registrationpage.ContinueOnlineOrderClick();
                })
                .InitPage(new NFZWelcomePage(driver), welcomepage =>
                {
                    welcomepage.Wait().WaitForReady(driver);
                    welcomepage.SubmitBtnClick();
                })
                .InitPage(new NFZContactDataPage(driver), contactDataPage =>
                {
                    contactDataPage.Wait().WaitForReady(driver);
                    contactDataPage.SelectPhoneCheckBox(driver);
                    contactDataPage.ClearContactFormWithPhoneField(driver);
                    Assert.That(contactDataPage.IsFirstNameErrorMsgDisplayed, Is.True, "FirstName error message is not displayed");
                    Assert.That(contactDataPage.FirstNameErrorMsg.Text, Is.EqualTo(Resources.FirstNameErrorMsg), "First name error message text is different");
                    Assert.That(contactDataPage.IsLastNameErrorMsgDisplayed, Is.True, "LastName error message is not displayed");
                    Assert.That(contactDataPage.LastNameErrorMsg.Text, Is.EqualTo(Resources.LastNameErrorMsg), "Last name error message text is different");
                    Assert.That(contactDataPage.IsEmailErrorMsgDisplayed, Is.True, "Email error message is not displayed");
                    Assert.That(contactDataPage.EmailErrorMsg.Text, Is.EqualTo(Resources.EmailErrorMsg), "Email error message text is different");
                    Assert.That(contactDataPage.IsTelephoneErrorMsgDisplayed, Is.True, "Phone number error message is not displayed");
                    Assert.That(contactDataPage.TelephoneErrorMsg.Text, Is.EqualTo(Resources.TelephoneNumberErrorMsg), "Telephone error message text is different");
                    contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
                    contactDataPage.FillInContactFormWithPhone(driver);
                })
                .InitPage(new NFZPaymentPage(driver), paymentPage =>
                {
                    paymentPage.Wait().WaitForReady(driver);
                    price = paymentPage.GetPriceValue(driver);
                    paymentPage.SubmitPayment();
                })
                .InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
                {
                    deliveryPage.Wait().WaitForReady(driver);
                    deliveryPage.ClearDeliveryForm();
                    deliveryPage.FillInDeliveryForm(driver);
                    deliveryPage.OpenBillindAddressModal(driver).ClearBillingForm(driver).FillInDeliveryForm(driver);
                    Thread.Sleep(2000);
                    deliveryPage.SubmitForm();
                })
    .InitPage(new NFZOrderOverviewPage(driver), orderOverviewPage =>
    {
        orderOverviewPage.Wait().WaitForReady(driver);
        Assert.That(orderOverviewPage.RetrieveContactDataValues(3), Is.EqualTo(Resources.TelephoneNumber), "Telephone number is incorrect");
        Assert.That(orderOverviewPage.RetrieveContactDataValues(1), Is.EqualTo(Resources.Email), "Email address is incorrect");
        Assert.That(orderOverviewPage.RetrievePaymentDataValues(3), Is.EqualTo(price + Resources.BruttoPriceCommercialPrice), "Car price is not displayed");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.FirstName + Resources.SecondName),
            "Salutation, Title, First name or Second name is not correct");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(1), Is.EqualTo(Resources.Street + Resources.HouseNumber),
            "Street or House number is not correct");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(3), Is.EqualTo(Resources.ZipCode + Resources.City),
            "ZipCode or City is not correct");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.BillingFirstName + Resources.BillingSecondName),
            "Salutation, Title, First name or Second name is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(1), Is.EqualTo(Resources.BillingStreet + Resources.BillingHouseNumber),
            "Street or House number is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(3), Is.EqualTo(Resources.BillingZipCode + Resources.BillingCity),
            "ZipCode or City is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");

        orderOverviewPage.SubmitDeliveryRemarks();
        Thread.Sleep(1000);
    }).InitPage(new NFZConfirmationPage(driver), confirmationPage =>
    {
        confirmationPage.Wait().WaitForReady(driver);
        Assert.That(confirmationPage.RetrievePrice(), Is.EqualTo(price + Resources.BruttoPriceCommercialPrice),
            "Car price is not displayed");
    });
        }
    }
}
