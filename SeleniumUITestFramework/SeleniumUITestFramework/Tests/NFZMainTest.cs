﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using RestSharp;
using System.Web.Script.Serialization;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using SeleniumUITestFramework.Controls;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Tests
{
    public class NFZMainTest : OSMainTest
    {
        public NFZMainTest() : base()
        {
            SetCarKey("?key=REVVMDE5MzcxMTUxNjM%3D");
        }

        public override string GetCarKey()
        {
            return CarKey;
        }

        public override String GetEnvUrl(Env env = Env.Dev)
        {
            switch (env)
            {
                case Env.Dev:
                    return getAppSettings("NFZDevHomePageURL");
                case Env.Stage:
                    return getAppSettings("NFZStageHomePageURL");
                case Env.Prelive:
                    return getAppSettings("NFZPreLiveHomePageURL");
                default:
                    return "environment not found";
            }
            
        }

        public override string GetAccessToSite()
        {
            String userName = "watosuser";
            String password = "SU392per";
            return userName + ":" + password + "@";
        }

        public override int UnlockCurrentCar()
        {
            String carKey = CarKey;
            //! remove '?=key' from CarKey variable
            int index = carKey.LastIndexOf("?key=");
            if (index > -1)
            {
                carKey = carKey.Remove(0, index + 5);
            }
            Thread.Sleep(500);

            IRestResponse response1 = driver.ApiCall(SITE_URL, "api-dev/nfz/car-lock/" + carKey, Method.DELETE);
            int resposneStatusCode = (int)response1.StatusCode;
            return resposneStatusCode;
        }
        /// <summary>
        /// Open HomePage site url with credentials and after applies the CarKey, should be call after SetUpDriver method
        /// </summary>
        public override void OpenHomePage()
        {
            String MainPageUrl = SITE_URL;
            int index = MainPageUrl.LastIndexOf("://");
            String AccessToSite = getAppSettings("NFZAccessToSite");
            if (TestedEnv == Env.Prelive) AccessToSite = getAppSettings("NFZPreLiveAccessToSite");
           
            String MainPageUrlWithAccess = MainPageUrl.Insert(index + 3, AccessToSite);

            GetDriver().Navigate().GoToUrl(MainPageUrlWithAccess);
            GetDriver().Navigate().GoToUrl(MainPageUrl + CarKey);

            
        }
    }
}
