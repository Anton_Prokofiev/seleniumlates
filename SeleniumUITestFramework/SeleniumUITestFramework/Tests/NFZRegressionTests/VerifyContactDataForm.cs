﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;


namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    public class VerifyContactDataForm : NFZMainTest
    {

        [Test(Description =
            "Verify BreadCrumb Flow")]
        [Ignore("In progress of development")]
        [TestCase("Chrome")]
        [TestCase("FireFox")]
        public void VerifyDropDownValues(string BrowserName)
        {
            SetCarKey("?key=REVVMDE5MzcxMTUxNjM%3D");
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new NFZHomePage(driver), homepage =>
            {
            homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
            homepage.GewerbeTileClick();
            homepage.OnlinePurchaseBtnClick(driver);

            }).InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
            {

            }).InitPage(new NFZContactDataPage(driver), contactdatapage =>
            {

            });
        }
    }
}