﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumUITestFramework.Controls.LightBoxes;
using SeleniumUITestFramework.PagesObjects.NFZ;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Tests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyDeliveryForm : NFZMainTest
    {

        [Test(Description =
            "Verify that delivery form has contact predefined values")]
        [TestCase("Chrome")]
        public void VerifyThatDeliveryFormHasContactPredefinedValues(string BrowserName)
        {
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
                .InitPage(new NFZHomePage(driver), homepage =>
                {
                    homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
                    homepage.GewerbeTileClick();
                    homepage.OnlinePurchaseBtnClick(driver);
                })
                .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
                {
                    loginpage.LoginWithParameters(ConfigurationManager.AppSettings["SecondUserLogin"],
                        ConfigurationManager.AppSettings["SecondUserPwd"]);
                })
                .InitPage(new NFZRegistrationPage(driver), registrationpage =>
                {
                    registrationpage.Wait().WaitForReady(driver);
                    registrationpage.ContinueOnlineOrderClick();
                }).InitPage(new NFZWelcomePage(driver), welcomepage =>
                {
                    welcomepage.Wait().WaitForReady(driver);
                    welcomepage.SubmitBtnClick();
                })
                .InitPage(new NFZContactDataPage(driver), contactDataPage =>
                {
                    Thread.Sleep(2000);
                    contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
                    contactDataPage.ClearContactFormWithoutPhoneField(driver);
                    contactDataPage.FillInContactDataFormWithParam(ConfigurationManager.AppSettings["FirstName"],
                        ConfigurationManager.AppSettings["SecondName"], ConfigurationManager.AppSettings["SecondUserLogin"]);

                }).InitPage(new NFZPaymentPage(driver), paymentPage =>
                {
                    paymentPage.Wait().WaitForReady(driver);
                    paymentPage.SubmitPayment();

                })
                .InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
                {
                    deliveryPage.Wait().WaitForReady(driver);
                    Assert.That(deliveryPage.TitleValue(driver), Is.EqualTo(Resources.Title.TrimEnd(' ')));
                    Assert.That(deliveryPage.SalutationValue, Is.EqualTo(Resources.Salutation.TrimEnd(' ')));
                    Assert.That(deliveryPage.FirstNameValue, Is.EqualTo(Resources.FirstName.TrimEnd(' ')));
                    Assert.That(deliveryPage.LastNameValue, Is.EqualTo(Resources.SecondName.TrimEnd(' ')));
                });
        }

        [Test(Description =
            "Verify delivery form validation errors")]
        [TestCase("Chrome")]
        public void VerifyDeliveryFormValidationErrors(string BrowserName)
        {
            SetCarKey("?key=REVVMDE5MzcxMTUxNjM%3D");
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new NFZHomePage(driver), homepage =>
            {
                homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
                homepage.GewerbeTileClick();
                homepage.OnlinePurchaseBtnClick(driver);
            })
            .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
            {
                loginpage.LoginWithParameters(getAppSettings("SecondUserLogin"), getAppSettings("SecondUserPwd"));
                UnlockCurrentCar();
            })
            .InitPage(new NFZRegistrationPage(driver), registrationpage =>
            {
                registrationpage.Wait().WaitForReady(driver);
                registrationpage.ContinueOnlineOrderClick();
            }).InitPage(new NFZWelcomePage(driver), welcomepage =>
            {
                welcomepage.Wait().WaitForReady(driver);
                welcomepage.SubmitBtnClick();
            }).InitPage(new NFZContactDataPage(driver), contactDataPage =>
            {
                contactDataPage.Wait().WaitForReady(driver);
                contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
                contactDataPage.ClearContactFormWithoutPhoneField(driver);
                contactDataPage.FillInContactDataFormWithParam(getAppSettings("FirstName"), getAppSettings("SecondName"), getAppSettings("SecondUserLogin"));
            }).InitPage(new NFZPaymentPage(driver), paymentPage =>
            {
                paymentPage.Wait().WaitForReady(driver);
                Thread.Sleep(500);
                paymentPage.SubmitPayment();
            })
            .InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
            {
                //Thread.Sleep(1000);
                deliveryPage.ClearDeliveryForm();
                Assert.That(deliveryPage.IsCityErrorMsgDisplayed, Is.True, "City error message is not displayed");
                Assert.That(deliveryPage.CityErrorMsg, Is.EqualTo(Resources.CityErrorMsg), "City error message has incorrect text");
                Assert.That(deliveryPage.IsStreetErrorMsgDisplayed, Is.True, "Street error message is not displayed");
                Assert.That(deliveryPage.StreetErrorMsg.Value.Text, Is.EqualTo(Resources.StreetErrorMsg), "Street error message has incorrect text");
                Assert.That(deliveryPage.IsFirstNameErrorMsgDisplayed, Is.True, "FirstName error message is not displayed");
                Assert.That(deliveryPage.FirstNameErrorMsg.Value.Text, Is.EqualTo(Resources.FirstNameErrorMsg), "FirstName error message has incorrect text");
                Assert.That(deliveryPage.IsLastNameErrorMsgDisplayed, Is.True, "LastName error message is not displayed");
                Assert.That(deliveryPage.LastNameErrorMsg.Value.Text, Is.EqualTo(Resources.LastNameErrorMsg), "LastName error message has incorrect text");
                Assert.That(deliveryPage.IsHouseNumberErrorMsgDisplayed, Is.True, "Hause Number error message is not displayed");
                Assert.That(deliveryPage.HouseNumberErrorMsg.Value.Text, Is.EqualTo(Resources.HouseNumberErrorMsg), "Hause Number error message has incorrect text");
                Assert.That(deliveryPage.IsPostalCodeErrorMsgDisplayed, Is.True, "Postal code error message is not displayed");
                Assert.That(deliveryPage.PostalCodeErrorMsg.Value.Text, Is.EqualTo(Resources.PostalCodeErrorMsg), "Postal Code error message has incorrect text");
            });
        }
    }
}