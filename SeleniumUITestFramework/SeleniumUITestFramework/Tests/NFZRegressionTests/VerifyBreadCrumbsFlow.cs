﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.BreadCrumbs;
using SeleniumUITestFramework.PagesObjects;

namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyBreadCrumbsFlow : NFZMainTest
    {
        [Test(Description =
            "Verify BreadCrumb Flow")]
        [TestCase("Chrome")]
        [TestCase("Firefox")]
        public void VerifyBreadCrumbsFlows(string BrowserName)
        {
            SetCarKey("?key=REVVMDE5MzcxMTUxNjM%3D");
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new NFZHomePage(driver), homepage =>
            {
                homepage.Wait().WaitForReady(driver);
                homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
                homepage.GewerbeTileClick();
                homepage.OnlinePurchaseBtnClick(driver);
            })
            .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
            {
                loginpage.LoginWithParameters(ConfigurationManager.AppSettings["SecondUserLogin"], ConfigurationManager.AppSettings["SecondUserPwd"]);
                int statusCode = UnlockCurrentCar();
                Console.WriteLine("API unblock car response code = " + statusCode);
                String URL = ConfigurationManager.AppSettings["NFZContactPageURL"];
                driver.Navigate().GoToUrl(URL);
            })
            .InitPage(new NFZContactDataPage(driver), contactDataPage =>
            {
                contactDataPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList(driver);
                TestBreadCrumb.Init(driver);
                Assert.That(TestBreadCrumb.waitForValidBreadcrumbs(1), Is.True, "Breadcrumb has incorrect status value");
                contactDataPage.SelectValuesFromDropDownMenu(driver, "Prof.", "Herr");
                contactDataPage.ClearContactFormWithoutPhoneField(driver);
                contactDataPage.FillInContactDataFormWithParam(ConfigurationManager.AppSettings["FirstName"],
                    ConfigurationManager.AppSettings["SecondName"], ConfigurationManager.AppSettings["SecondUserLogin"]);

            }).InitPage(new NFZPaymentPage(driver), paymentPage =>
            {
                paymentPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList(driver);
                TestBreadCrumb.Init(driver);
                Assert.That(TestBreadCrumb.waitForValidBreadcrumbs(2), Is.True, "Breadcrumb has incorrect status value");
                paymentPage.SubmitPayment();

            }).InitPage(new NFZDeliveryAddressPage(driver), deliveryPage =>
            {
                deliveryPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList(driver);
                TestBreadCrumb.Init(driver);
                Assert.That(TestBreadCrumb.waitForValidBreadcrumbs(3), Is.True, "Breadcrumb has incorrect status value");
                deliveryPage.ClearDeliveryForm();
                deliveryPage.FillInDeliveryForm(driver);
                deliveryPage.SubmitForm();

            }).InitPage(new NFZOrderOverviewPage(driver), orderOverviewPage =>
            {
                orderOverviewPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList(driver);
                TestBreadCrumb.Init(driver);
                Assert.That(TestBreadCrumb.waitForValidBreadcrumbs(4), Is.True, "Breadcrumb has incorrect status value");
            });
        }
    }
}
