﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using SeleniumUITestFramework.PagesObjects;


namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyPrivacyAndTermsOfUseLinks : NFZMainTest
    {

        [Test(Description =
            "Verify that Data protection policy and Terms of Use links are leading to correct pages")]
        public void VerifyThatDataProtectionPolicyAndTermsOfUseLinksAreLeadingToCorrectPagesChrome()
        {

            RemoteWebDriver driver = DriverSetUp();
            driver
                .InitPage(new NFZHomePage(driver), homepage =>
                {
                    Thread.Sleep(2000);
                    LightBox LightBox = homepage.OrderOnlineNowBtnClick(driver);
                    LightBox.OpenDataProtectionPolicy();
                    String DataProtectionPage = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
                    Assert.That(DataProtectionPage, Is.EqualTo(ConfigurationManager.AppSettings["NFZDataProtectionPolicyURL"]));
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                    LightBox.OpenTermsOfUse();
                    String TermsOfUse = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
                    Assert.That(TermsOfUse, Is.EqualTo(ConfigurationManager.AppSettings["NFZTermsOfUseURL"]));
                    
                });
        }
    }
}
