﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapyFramework.Test;

namespace SeleniumUITestFramework.Tests
{
    public abstract class OSMainTest : MainTest
    {
        public String CarKey { get; private set; }

        protected OSMainTest()
        {
           SetTestedEnv(Env.Stage);
            
           SITE_URL = GetEnvUrl(TestedEnv);
        }
        
        public abstract String GetCarKey();
        public abstract int UnlockCurrentCar();
        public void SetCarKey(String carKey)
        {
            this.CarKey = carKey;
        }
    }
}
