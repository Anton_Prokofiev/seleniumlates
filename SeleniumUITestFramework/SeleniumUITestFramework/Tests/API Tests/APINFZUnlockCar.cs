﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using SeleniumUITestFramework.PagesObjects.PKW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumUITestFramework.Tests.API_Tests
{
    class APINFZUnlockCar : NFZMainTest
    {
        [Test(Description =
       "NFZ Unlock car with API call")]
        [TestCase("?key=REVVMDE5MzcxMzAyNTY=")]
        [Category("API")]
        public void UnlockCarByApiCallNFZ(String carKey)
        {
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp();
            driver
                .InitPage(new NFZHomePage(driver), homepage =>
                {
                    homepage.Wait().WaitForReady(driver);
                    //                    homepage.CookieNotificationClose();
                    homepage.OrderOnlineNowBtnClick(driver).SubmitDataPolicyAgreement();
                    homepage.GewerbeTileClick();
                    homepage.OnlinePurchaseBtnClick(driver);
                })
                .InitPage(new NFZVolkswagenLoginPage(driver), loginpage =>
                {
                    loginpage.Login();
                    int responseCode = UnlockCurrentCar();
                    Assert.That(responseCode == 200 || responseCode == 422, "response code isn't correct " + responseCode);
                    Console.WriteLine("response code = " + responseCode);
                });       

        }
    }
}
