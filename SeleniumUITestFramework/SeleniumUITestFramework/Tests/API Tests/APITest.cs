﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;

namespace SeleniumUITestFramework.Tests.API_Tests
{
    [TestFixture]
    [Category("API")]
    class APITest
    {
        [Test(Description =
            "API GET example test")]

        public void APIGETTests()
        {
            var client = new RestClient("https://postman-echo.com");
            var request = new RestRequest("get?foo1=bar1&foo2=bar2", Method.GET);
            var response = client.Execute(request);
           
/*          var deserialize = new JsonDeserializer();
            var output = deserialize.Deserialize<Dictionary<string, string>>(response);
            var result = output["url"];
*/          

            JObject obs = JObject.Parse(response.Content);
            Assert.That(obs["url"].ToString(), Is.EqualTo("https://postman-echo.com/get?foo1=bar1&foo2=bar2"));
        }

        [Test(Description =
            "API POST example test")]

        public void APIPOSTTests()
        {
            var client = new RestClient("http://vwos-dev.intern.etecture.de:9009/");
            client.Authenticator = new HttpBasicAuthenticator("awsi-notification-user", "strenggeheim");

            var request = new RestRequest("v1/commissions-external", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(
                new
                {
                    dealerId = "DEU52175",
                    commissionId = "VDV141",
                    vin = "WVWZZZAUZJW187116",
                    systemId = "xxx",
                    status = "SOLDACK"
                }); 
                
            var response = client.Execute(request).StatusCode;
            int numericStatusCode = (int)response;
            Console.WriteLine(numericStatusCode);


//            JObject obs = JObject.Parse(response.Content);
//            Assert.That(obs["url"].ToString(), Is.EqualTo(""));

        }
    }
}
