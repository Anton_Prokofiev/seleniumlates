﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using SeleniumUITestFramework.PagesObjects.PKW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumUITestFramework.Tests.API_Tests
{
    class APIPKWUnlockCar : PKWMainTest
    {
        [Test(Description =
               "PKW Unlock car with API call")]
        [TestCase("?key=REVVNDUwODBWRFYxNzY=")]
        [Category("API")]
        public void UnlockCarByApiCallPKW(String carKey)
        {
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp();
            driver
                .InitPage(new PKWHomePage(driver), homepage =>
                {
                    homepage.Wait().WaitForReady(driver);
                    homepage.OrderOnlineNowBtnClick().SubmitDataPolicyAgreement();
                }).InitPage(new PKWVolkswagenLoginPage(driver), loginpage =>
                {
                    loginpage.Login();
                    int responseCode = UnlockCurrentCar();
                    Assert.That(responseCode == 200 || responseCode == 422, "response code isn't correct " + responseCode);
                    Console.WriteLine("response code = " + responseCode);
                });

        }
    }
}
