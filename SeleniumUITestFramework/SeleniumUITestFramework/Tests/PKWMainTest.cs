﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.Tests
{
    public class PKWMainTest : OSMainTest
    {
        public PKWMainTest() : base()
        {
            SetCarKey("?key=REVVNDUwODBWRFYxNzY=");
        }

        public override string GetCarKey()
        {
            return CarKey;
        }

        public override String GetEnvUrl(Env env = Env.Dev)
        {
            switch (env)
            {
                case Env.Dev:
                    return getAppSettings("PKWDevHomePageURL");
                case Env.Stage:
                    return getAppSettings("PKWStageHomePageURL");
                case Env.Prelive:
                    return getAppSettings("PKWPreLiveHomePageURL");
                default:
                    return "environment not found";
            }
        }
        public override string GetAccessToSite()
        {
            String userName = "watosuser";
            String password = "SU392per";
            return userName + ":" + password + "@";
        }

        public override int UnlockCurrentCar()
        {
            String carKey = CarKey;
            //! remove '?=key' from CarKey variable
            int index = carKey.LastIndexOf("?key=");
            if (index > -1)
            {
                carKey = carKey.Remove(0, index + 5);
            }

            Thread.Sleep(500);

            IRestResponse response1 = driver.ApiCall(SITE_URL, "api-dev/pkw/car-lock/" + carKey, Method.DELETE);
            int resposneStatusCode = (int)response1.StatusCode;
            return resposneStatusCode;
        }
        /// <summary>
        /// Open HomePage site url with credentials and after applies the CarKey, should be call after SetUpDriver method
        /// </summary>
        public override void OpenHomePage()
        {
            String MainPageUrl = SITE_URL;
            int index = MainPageUrl.LastIndexOf("://");
            String AccessToSite = getAppSettings("PKWAccessToSite");
            if (TestedEnv == Env.Prelive) AccessToSite = getAppSettings("PKWPreLiveAccessToSite");
            String MainPageUrlWithAccess = MainPageUrl.Insert(index + 3, AccessToSite);

            GetDriver().Navigate().GoToUrl(MainPageUrlWithAccess);
            GetDriver().Navigate().GoToUrl(MainPageUrl + CarKey);
        }
    }
}
