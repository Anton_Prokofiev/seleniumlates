﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using SeleniumUITestFramework.PagesObjects.PKW;
using System.Threading;

namespace SeleniumUITestFramework.Tests.PKWSmokeTest.RegisteredUserFlow
{
    [Category("Smoke PKW")]
    class VerifyThatUserCanWalkThroughFSAGviaMockPage : PKWMainTest
    {

        [Test(Description =
                "Verify that user can successfully walk-through 'Finanzierung' payment 'Auslieferung' delivery flow (already registered user)")]
        [TestCase("?key=REVVNDUwODBWRFYyNDA=", "Chrome")]
        public void VerifyThatUserCanWalkThroughFinancingPaymentAuslieferungDeliveryFlowViaMock(String carKey, string BrowserName)
        {
            SetCarKey(carKey);
            RemoteWebDriver driver = DriverSetUp(BrowserName);
            driver
            .InitPage(new PKWHomePage(driver), homepage =>
            {
                homepage.Wait().WaitForReady(driver);
                homepage.OrderOnlineNowBtnClick().SubmitDataPolicyAgreement();
            }).InitPage(new PKWVolkswagenLoginPage(driver), loginpage =>
            {
                loginpage.Login();
                int carUnblockResposeCode = UnlockCurrentCar();
                Console.WriteLine("car unblocked and ruturned status code = " + carUnblockResposeCode);
            })
            .InitPage(new PKWPaymentMethodSelectionPage(driver), paymentSelectionPage =>
            {
                // TODO this step doesn't need - checkbox prefilled on page load
                // paymentSelectionPage.ClickOnMailChkBox();
                Thread.Sleep(200);
                paymentSelectionPage.ClickOnPrivacyPolicyChkBox();
                paymentSelectionPage.ClickOnFinancingPaymentBtn().PKWPaimentMethodConfirm(driver);
                Console.WriteLine("clicked");
            })
            .InitPage(new MockFSAGPage(driver), mockPage =>
            {
                mockPage.IsFsagResponceBlockDisplayed();
                Thread.Sleep(1000); // compelled wait for automatically fill all fields
                mockPage.ClickOnSendBtn();
            })
            .InitPage(new PKWAdditionalServiceSummaryPage(driver), additionalService =>
            {
                additionalService.ClickOnAuslieferungDeliveryMethod();
                additionalService.FillLicensePlateFields();
                additionalService.ChooseTheDateFromCelendar();
                Thread.Sleep(200);
                additionalService.ClickOndeliveryDateOneMorningChkbox();
                additionalService.ClickOnSubmitBtn();

            }).InitPage(new PKWPaymentPage(driver), paymentPage =>
            {
                paymentPage.ChoosePaymentMethodByBankTransfer();
            }).InitPage(new PKWOrderSuccessPage(driver), orderSuccessPage =>
            {
                Assert.That(orderSuccessPage.IsOrderSuccessPageSectionDisplayed, "Order success page isn't displayed");
                UnlockCurrentCar();
            });
        }
    }
}
