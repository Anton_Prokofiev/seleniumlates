﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CapyFramework.Page;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using SeleniumUITestFramework.PagesObjects.PKW;


namespace SeleniumUITestFramework.Tests.PKWSmokeTest.RegisteredUserFlow
{
    class VerifyThatUSerCanWalkThroughDirectPaymentDeliveryFlow
    {
        [Category("Smoke PKW")]
        public class VerifyPKWPrivatDirektbezahlungFlow : PKWMainTest
        {
            public VerifyPKWPrivatDirektbezahlungFlow() : base()
            {
            }

            [Test(Description =
                "Verify that user can successfully walk-through Direct payment 'Auslieferung' delivery flow (already registered user)")]
            [TestCase("?key=REVVNDUwODBWRFYyMzQ%3D", "Chrome")]
            public void VerifyThatUserCanWalkThroughDirectPaymentAuslieferungDeliveryFlow(String carKey, string BrowserName)
            {
                SetCarKey(carKey);
                RemoteWebDriver driver = DriverSetUp(BrowserName);
                driver
                .InitPage(new PKWHomePage(driver), homepage =>
                {
                    homepage.Wait().WaitForReady(driver);
                    homepage.OrderOnlineNowBtnClick().SubmitDataPolicyAgreement();
                }).InitPage(new PKWVolkswagenLoginPage(driver), loginpage =>
                {
                    loginpage.Login();
                    UnlockCurrentCar();
                    Console.WriteLine("car unblocked");
                })
                .InitPage(new PKWPaymentMethodSelectionPage(driver), paymentSelectionPage =>
                {
                    paymentSelectionPage.ClickOnDirectPaymentBtn();
                    paymentSelectionPage.IsPrivacyPolicyErrMsgIsDisplayed();
                    Thread.Sleep(200);
                    //paymentSelectionPage.ClickOnMailChkBox();
                    paymentSelectionPage.ClickOnPrivacyPolicyChkBox();
                    //paymentSelectionPage.IsEmailInputFieldEqualVWID(); // TODO can't get the text from input email input field
                    paymentSelectionPage.ClickOnDirectPaymentBtn();
                }).InitPage(new PKWAdditionalServiceSummaryPage(driver), additionalService =>
                {
                    additionalService.ClickOnAuslieferungDeliveryMethod();
                    additionalService.FillLicensePlateFields();
                    additionalService.ChooseTheDateFromCelendar();
                    Thread.Sleep(200);
                    additionalService.ClickOndeliveryDateOneMorningChkbox();
                    //additionalService.OpenBillindAddressModal().ClearBillingForm(driver).PKWFillInDeliveryForm(driver);
                    additionalService.ClickOnSubmitBtn();

                }).InitPage(new PKWPaymentPage(driver), paymentPage =>
                {
                    paymentPage.ChoosePaymentMethodByBankTransfer();
                }).InitPage(new PKWOrderSuccessPage(driver), orderSuccessPage =>
                {
                    Assert.That(orderSuccessPage.IsOrderSuccessPageSectionDisplayed, "Order success page isn't displayed");
                    UnlockCurrentCar();
                });
            }
            [Test(Description =
                "Verify that user can successfully walk-through Direct payment 'Abholung' delivery flow (already registered user)")]
            [TestCase("?key=REVVNDUwODBWRFYxNzY=", "Chrome")]
            public void VerifyThatUserCanWalkThroughDirectPaymentAbholungDeliveryFlow(String carKey, string BrowserName)
            {

                SetCarKey(carKey);
                RemoteWebDriver driver = DriverSetUp(BrowserName);
                driver
                .InitPage(new PKWHomePage(driver), homepage =>
                {
                    homepage.Wait().WaitForReady(driver);
                    homepage.OrderOnlineNowBtnClick().SubmitDataPolicyAgreement();
                }).InitPage(new PKWVolkswagenLoginPage(driver), loginpage =>
                {
                    loginpage.Login();
                    int responseCode = UnlockCurrentCar();
                    Console.WriteLine(responseCode);
                })
                .InitPage(new PKWPaymentMethodSelectionPage(driver), paymentSelectionPage =>
                {
                    paymentSelectionPage.ClickOnDirectPaymentBtn();
                    paymentSelectionPage.IsPrivacyPolicyErrMsgIsDisplayed();
                    Thread.Sleep(200);
                    // TODO this step doesn't need - checkbox prefilled on page load
                    // paymentSelectionPage.ClickOnMailChkBox();
                    paymentSelectionPage.ClickOnPrivacyPolicyChkBox();
                    //paymentSelectionPage.IsEmailInputFieldEqualVWID(); // TODO can't get the text from input email input field
                    paymentSelectionPage.ClickOnDirectPaymentBtn();
                }).InitPage(new PKWAdditionalServiceSummaryPage(driver), additionalService =>
                {
                    additionalService.ClickOnAbholungDeliveryMethod();
                    additionalService.FillLicensePlateFields();
                    additionalService.ChooseTheDateFromCelendarAbholung();
                    additionalService.ChooseDeliveryTime();
                    additionalService.ClickOnSubmitBtn();
                }).InitPage(new PKWPaymentPage(driver), paymentPage =>
                {
                    paymentPage.ChoosePaymentMethodByBankTransfer();
                }).InitPage(new PKWOrderSuccessPage(driver), orderSuccessPage =>
                {
                    Assert.That(orderSuccessPage.IsOrderSuccessPageSectionDisplayed, "Order success page isn't displayed");
                    UnlockCurrentCar();
                });
            }
        }
    }
}
