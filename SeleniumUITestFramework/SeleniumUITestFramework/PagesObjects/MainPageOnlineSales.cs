﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using CapyFramework.Page;
using CapyFramework.Helpers;

namespace SeleniumUITestFramework.PagesObjects
{
    class MainPageOnlineSales : MainPage
    {
        protected String CarKey;
        public MainPageOnlineSales(RemoteWebDriver driver) : base(driver)
        {
            
        }

        public void SetCarKey(String carKey)
        {
            this.CarKey = carKey;
        }

        public WaitHelper Wait()
        {
            return new WaitHelper(driver);
        }

    }
}
