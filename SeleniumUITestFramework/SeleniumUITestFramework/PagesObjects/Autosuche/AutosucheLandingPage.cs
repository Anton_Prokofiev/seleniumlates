﻿using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects
{
    class AutosucheLandingPage : MainPageOnlineSales
    {
        private IWebElement ModelDropDownBtn;
        private Lazy<IList<IWebElement>> ModelDropDownList;
        private IWebElement PriceDropDownBtn;
        private Lazy<IList<IWebElement>> PriceDropDownList;
        private IWebElement RegistrationDateDropDownBtn;
        private Lazy<IList<IWebElement>> RegistrationDateDropDownList;
        private IWebElement EquipmentDropDownBtn;
        private Lazy<IList<IWebElement>> EquipmentDropDownList;
        private IWebElement KilometerToDropDownBtn;
        private Lazy<IList<IWebElement>> KilometerToDropDownList;
        private IWebElement FuelDropDownBtn;
        private Lazy<IList<IWebElement>> FuelDropDownList;
        private IWebElement LocationTextInputField;
        private IWebElement SearchRadiusDropDownBtn;
        private Lazy<IList<IWebElement>> SearchRadiusDropDownList;
        private IWebElement SearchBtn;

        public AutosucheLandingPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)   // Init Web elements due to obsoleted PageFactory
        {
            ModelDropDownBtn = driver.FindElementByXPath("//div[@id='id-1-super-select']");
            PriceDropDownBtn = driver.FindElementByXPath("//div[@id='id-2-super-select']");
            RegistrationDateDropDownBtn = driver.FindElementByXPath("//div[@id='id-3-super-select']");
            EquipmentDropDownBtn = driver.FindElementByXPath("//div[@id='id-4-super-select']");
            KilometerToDropDownBtn = driver.FindElementByXPath("//div[@id='id-5-super-select']");
            FuelDropDownBtn = driver.FindElementByXPath("//div[@id='id-6-super-select']");
            LocationTextInputField = driver.FindElementByXPath("//input[@id='id-9-input']");
            SearchRadiusDropDownBtn =  driver.FindElementByXPath("//div[@id='id-8-super-select']");
            SearchBtn = driver.FindElementByXPath("//a[contains(@class,'autosuche__button autosuche__button--primary autosuche__button--icon autosuche__landing-page__search-section__button')]");

            ModelDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));
            PriceDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));
            RegistrationDateDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));
            EquipmentDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));
            KilometerToDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));
            FuelDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));
            SearchRadiusDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByXPath("//li[@class='r-ss-dropdown-option']"));

        }

        public bool IsPageLoaded => ModelDropDownBtn.Displayed;


        public void InitAutoSearch(string model, string price)
        {
            ModelDropDownBtn.Click();      
            ModelDropDownList.Value.First(x => x.Text.Contains(model)).Click();

            PriceDropDownBtn.Click();
            PriceDropDownList.Value.First(x => x.Text.Contains(price)).Click(); 
            
            RegistrationDateDropDownBtn.Click();
            RegistrationDateDropDownList.Value[5].Click();

            SearchBtn.Click();
        }

        public void Open(RemoteWebDriver driver)
        {
            var URL = ConfigurationManager.AppSettings["SearchPageURL"];
            driver.Navigate().GoToUrl(URL);          
        }     
    }
}
