﻿using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects
{
    class SearchResultPage : MainPageOnlineSales
    {
        private IWebElement AutosearchResultSection;

        public SearchResultPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            AutosearchResultSection = driver.FindElementByXPath("//ul[contains(@class,'autosuche__selected-criterias autosuche__selected-criterias__togglesection')]");

        }

        public bool IsSearchResultPageDisplayed => AutosearchResultSection.Displayed;

        public void Open(RemoteWebDriver driver)
        {

        }
    }
}
