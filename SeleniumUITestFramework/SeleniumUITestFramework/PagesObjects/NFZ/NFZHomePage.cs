﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using CapyFramework.Helpers;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZHomePage : MainPageOnlineSales
    {
        private Lazy<IWebElement> OrderOnlineNowBtn;
        private Lazy<IWebElement> OnlinePurchaseBtn;
        private Lazy<IWebElement> GewerbeBtn;
        private Lazy<IWebElement> DigitalSellerLink;
        private Lazy<IWebElement> CookieNotificationCloseBtn;

        public NFZHomePage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            OrderOnlineNowBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-stage .os-button"));
            OnlinePurchaseBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-button--primary"));
            GewerbeBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #COMMERCIAL"));
            DigitalSellerLink = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-app-header-nfz__action-list__item--contact"));
            CookieNotificationCloseBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".vw_m641_system_notification_close"));
        }

        public void Open(RemoteWebDriver driver)
        {
        }

        public bool IsPageLoaded => OrderOnlineNowBtn.Value.Displayed;
        public void CookieNotificationClose()
        {
            CookieNotificationCloseBtn.Value.Click();
        }

        public LightBox OpenDigitalSellerLightBox(RemoteWebDriver driver)
        {
            DigitalSellerLink.Value.Click();
            return driver.CreateLightBox<LightBox>();
        }
        public LightBox OrderOnlineNowBtnClick(RemoteWebDriver driver)
        {
            S("//button[@id='landing-page-next-c2a']").clickOnElement("can't click on 'Fahrzeug jetzt online bestellen' button");
            return driver.CreateLightBox<LightBox>();
        }
        

        public void OnlinePurchaseBtnClick(RemoteWebDriver driver)
        {
            OnlinePurchaseBtn.Value.Click();
        }

        public void GewerbeTileClick()
        {
            GewerbeBtn.Value.Click();
        }
        /// <summary>
        /// Click on Direct payment method label
        /// </summary>
        public void DirectPaymentTileClick()
        {
            S("//label[@id='DIRECT_PAYMENT']").clickOnElementWithWait(driver, "can't click on 'Direktbezahlung' button");
        }
        /// <summary>
        /// Click on Finanicing payment method label
        /// </summary>
        public void FinancingMethodClick()
        {
            S("//label[@id='FINANCING']//span[@class='os-radio-tile__container']").clickOnElementWithWait(driver);
        }
    }
}
