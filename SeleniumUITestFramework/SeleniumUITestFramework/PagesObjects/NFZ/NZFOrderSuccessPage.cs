﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;



namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NZFOrderSuccessPage : MainPageOnlineSales
    {
        By OrderSuccesSectionLocator = By.XPath("//div[contains(@class,'os-congratulations')]");
        public bool IsOrderSuccessSectionDisplayed => S(OrderSuccesSectionLocator).Displayed;
        public NZFOrderSuccessPage(RemoteWebDriver driver) : base(driver)
        {
        }
    }
}
