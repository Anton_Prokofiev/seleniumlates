﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NFZConfirmationPage : MainPageOnlineSales
    {
        private Lazy<IWebElement> Price;

        public NFZConfirmationPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Open(RemoteWebDriver driver)
        {
        }

        public void Init(RemoteWebDriver driver)
        {
            Price = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-car-info__info-block .os-car-info__info-block__financing-copy > span"));         
        }
        public bool IsPageLoaded => Price.Value.Displayed;
        public string RetrievePrice()
        {
            var p = Price.Value.Text;
            return p;
        }
    }
}
