﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using CapyFramework.Helpers;

namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NFZRegistrationPage : MainPageOnlineSales

    {
        private Lazy<IWebElement> ContinueOnlineOrderBtn;

        public NFZRegistrationPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            ContinueOnlineOrderBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-registration-view__button.os-button.os-button--primary"));
        }
        public void ContinueOnlineOrderClick()
        {
            S("//button[contains(@class ,'os-registration-view__button')]").clickOnElementWithWait(driver);
        }
    }
}
