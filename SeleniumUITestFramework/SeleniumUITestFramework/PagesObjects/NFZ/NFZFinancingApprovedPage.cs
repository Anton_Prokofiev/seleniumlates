﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using CapyFramework.Helpers;
using CapyFramework.Helpers.Waiter;

namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NFZFinancingApprovedPage : MainPageOnlineSales
    {
        public NFZFinancingApprovedPage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void ClickOnNextBtn()
        {
            S("//a[@class='os-button os-button--primary os-handover-from-fsag__button os-navigation-link']").clickOnElementWithWait(driver);
        }
    }
}
