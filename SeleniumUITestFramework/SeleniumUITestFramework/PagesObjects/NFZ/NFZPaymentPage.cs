﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using System.Text.RegularExpressions;
using CapyFramework.Helpers;
using CapyFramework.Helpers.Waiter;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZPaymentPage : MainPageOnlineSales
    {
        private Lazy<IWebElement> Price;

        private By ContinueFinancingBtnLocator = By.XPath("//button[contains(@class,'os-financing-info__button-next')]");

        public NFZPaymentPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            Price = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-introduction .os-container .os-copy--1> strong"));
        }
        static int convertToInt(string a)
        {
            int x = 0;

            Char[] charArray = a.ToCharArray();
            int j = charArray.Length;

            for (int i = 0; i < charArray.Length; i++)
            {
                j--;
                int s = (int)Math.Pow(10, j);

                x += ((int)Char.GetNumericValue(charArray[i]) * s);
            }
            return x;
        }
        public void WaitForPaymentAmountExist()
        {
            float amount = getFloatFromElementText(By.XPath("//section[contains(@class,'os-introduction')]//p[1]/strong"), 10, "can't get text from PaymentAmount");

            //string str = "01567438absdg34590";
            

            //String resultWithComma = Regex.Replace(amount, @"[^\d\,]", "");
            //Console.WriteLine(resultWithComma);
            //String resultWithDots = Regex.Replace(resultWithComma, @"[\,]", ".");
            //Console.WriteLine(resultWithDots);

            //float Float = float.Parse(resultWithComma);
            Console.WriteLine(amount);
        }

        public void SubmitPayment()
        {
            S(By.CssSelector(".os-button__caption")).clickOnElement();
        }
        public void SubmitPaymentBtnClick()
        {
            S("//button[@id='delivery-address-form-c2a-submit']").clickOnElementWithWait(driver);
        }

        public string GetPriceValue(RemoteWebDriver driver)
        {
          /*  string s = Price.Value.Text;
            if (s.Contains("€"))
            { return s;}
            Thread.Sleep(2000);
            return s;
*/
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(d =>
            {
                Thread.Sleep(1500);
                try
                {
                    driver.FindElement(By.ClassName("os-loading-spinner os-loading-spinner--inline"));
                    return false;
                }
                catch
                {
                    return true;
                }
            });
            string s = Price.Value.Text;
            return s;
        }
        /// <summary>
        /// Click on confirm Financing button for FSAG process
        /// </summary>
        /// <param name="withShift">true for redirect to Mock FSAG page</param>
        public void FinancingContinueBtnClick(Boolean withShift = true)
        {
            if (withShift)
            {
                S(ContinueFinancingBtnLocator).waitForElementClickable(driver).shiftClick(driver);
            }
            else
            {
                S(ContinueFinancingBtnLocator).clickOnElementWithWait(driver);
            }
        }
    }
}
