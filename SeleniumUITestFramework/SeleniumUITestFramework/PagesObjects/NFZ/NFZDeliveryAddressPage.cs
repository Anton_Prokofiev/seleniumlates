﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using OpenQA.Selenium.Support.UI;
using CapyFramework.Helpers;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZDeliveryAddressPage : MainPageOnlineSales
    {
        public Lazy<IWebElement> FirstNameErrorMsg;
        public Lazy<IWebElement> LastNameErrorMsg;
        public Lazy<IWebElement> StreetErrorMsg;
        public Lazy<IWebElement> HouseNumberErrorMsg;
        public Lazy<IWebElement> PostalCodeErrorMsg;
        private Lazy<IWebElement> TitleDropDown;
        private Lazy<IWebElement> SalutationDropDown;
        private Lazy<IWebElement> BillingAddressLink;

        // delivery form locators
        By StreetTextFieldLocator = By.CssSelector("#Input--street");
        By HouseNumberTextFieldLocator = By.CssSelector("#Input--houseNumber");
        By CompanyTextFieldLocator = By.CssSelector("#Input--company");
        By ZipCodeTextFieldLocator = By.CssSelector("#Input--zipCode");
        By CityTextFieldLocator = By.CssSelector("#Input--city");
        By FirstNameTextFieldLocator = By.CssSelector("#Input--firstName");
        By LastNameTextFieldLocator = By.CssSelector("#Input--lastName");

        By CityErrorMsgLocator = By.CssSelector("[data-scroll-container='city'] .os-input__error");

        public string FirstNameValue => S(FirstNameTextFieldLocator).GetAttribute("value");
        public string LastNameValue => S(LastNameTextFieldLocator).GetAttribute("value");
        public string CityErrorMsg => S(CityErrorMsgLocator).Text;

        public bool IsFirstNameErrorMsgDisplayed => FirstNameErrorMsg.Value.Displayed;
        public bool IsLastNameErrorMsgDisplayed => LastNameErrorMsg.Value.Displayed;
        public bool IsCityErrorMsgDisplayed => S(CityErrorMsgLocator).Displayed;
        public bool IsPostalCodeErrorMsgDisplayed => PostalCodeErrorMsg.Value.Displayed;
        public bool IsStreetErrorMsgDisplayed => StreetErrorMsg.Value.Displayed;
        public bool IsHouseNumberErrorMsgDisplayed => HouseNumberErrorMsg.Value.Displayed;

        public NFZDeliveryAddressPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            TitleDropDown = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form [name='title']"));
            SalutationDropDown = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form [data-scroll-container='salutation'] .os-select__caption"));

            BillingAddressLink = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form .os-navigation-link__caption"));
            FirstNameErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='firstName'] .os-input__error"));
            LastNameErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='lastName'] .os-input__error"));
            StreetErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='street'] .os-input__error"));
            HouseNumberErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='houseNumber'] .os-input__error"));
            PostalCodeErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='zipCode'] .os-input__error"));

        }

        public void ClearDeliveryForm()
        {
            S(FirstNameTextFieldLocator, "can't click on FIRST_NAME text field", 5).clickOnElement().clear();

            S(LastNameTextFieldLocator, "can't click on LAST_NAME text field", 5).clickOnElement().clear();

            S(StreetTextFieldLocator, "can't click on Street text field", 5).clickOnElement().clear();
            S(HouseNumberTextFieldLocator).clickOnElement("can't click on HouseNumber field").clear();
            S(CompanyTextFieldLocator).clickOnElement("can't fill the Company field").clear();
            S(ZipCodeTextFieldLocator).clickOnElement("can't click on ZipCode field").clear();
            S(CityTextFieldLocator).clickOnElement("can't click on City field").clear();

            S(FirstNameTextFieldLocator).clickOnElement();
            S(CityErrorMsgLocator, "City error message isn't displayed", 5);
        }

        public void FillInDeliveryForm(RemoteWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,-900)");

            S(FirstNameTextFieldLocator, "can't click on FIRST_NAME text field", 5).clickOnElement().clearAndFill(getAppSettings("FirstName"));
            S(LastNameTextFieldLocator, "can't click on LAST_NAME text field", 5).clickOnElement().clearAndFill(getAppSettings("SecondName"));
            S(StreetTextFieldLocator).clickOnElement("can't click on street field").clearAndFill(getAppSettings("Street"));
            S(HouseNumberTextFieldLocator).clickOnElement("can't fill the HouseNumber field").clearAndFill(getAppSettings("HouseNumber"));
            S(CompanyTextFieldLocator).clickOnElement("can't fill the Company field").clearAndFill(getAppSettings("Company"));
            S(ZipCodeTextFieldLocator).clickOnElement("can't click on ZipCode field").clearAndFill(getAppSettings("ZipCode"));
            S(CityTextFieldLocator).clickOnElement("can't click on City field").clearAndFill(getAppSettings("City"));

        }

        public void SubmitForm()
        {
            S(By.CssSelector("#delivery-address-form-c2a-submit"), "can't click on submit button").clickOnElement();
        }

        public string TitleValue(RemoteWebDriver driver)
        {
            By selectLocator = By.CssSelector(".os-form__form [name='title']");
            SelectElement select = new SelectElement(S(selectLocator, "can't get the select field TITLE"));
            return select.SelectedOption.Text;
        }
        public string SalutationValue()
        {
            return SalutationDropDown.Value.Text;
        }

        public LightBox OpenBillindAddressModal(RemoteWebDriver driver)
        {
            BillingAddressLink.Value.Click();
            return driver.CreateLightBox<LightBox>();
        }

    }
}
