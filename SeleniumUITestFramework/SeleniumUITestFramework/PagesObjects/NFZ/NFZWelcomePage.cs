﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;



namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NFZWelcomePage : MainPageOnlineSales
    {
        private Lazy<IWebElement> SubmitBtn;

        public NFZWelcomePage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            SubmitBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-welcome-section__button"));            
        }

        public void SubmitBtnClick()
        {
            SubmitBtn.Value.Click();
        }
    }
}
