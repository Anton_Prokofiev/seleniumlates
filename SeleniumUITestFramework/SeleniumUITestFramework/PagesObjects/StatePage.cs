﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using RestSharp;
using RestSharp.Authenticators;

namespace SeleniumUITestFramework.PagesObjects
{
    class StatePage : MainPageOnlineSales
    {
        
        public StatePage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void UnblockCurrentCar(String siteUrl)
        {


            var client = new RestClient("https://qa.retailservices.audi.de/");
            //client.Authenticator = new HttpBasicAuthenticator("awsi-notification-user", "strenggeheim");


            String accessToken = driver.Manage().Cookies.GetCookieNamed("access-token").Value;
            var request = new RestRequest("vsbs/v1/commissions/DEU45080/VDV176/WVWZZZAUZJW174293/status", Method.PUT);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic dGVzdHVzZXIxOlRlc3RVc2VyMVBXIyE=");


            request.AddHeader("x-authorization", "Bearer eyJraWQiOiJmMDNhNjhjMTJjZmMzYmFiIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI1NWU4ODRkNi03ZGFkLTQ0OGItYmI5My1jYmQzYzM1YmExYTAiLCJhdWQiOiIzNDNlOTIwNy03YWEyLTQ0MWMtYjhlZC1iYjY4MzA1MDhlZDBAYXBwc192dy1kaWxhYl9jb20iLCJzY3AiOiJvcGVuaWQgcHJvZmlsZSBwaG9uZSBhZGRyZXNzIiwiYWF0IjoiaWRlbnRpdHlraXQiLCJpc3MiOiJodHRwczpcL1wvaWRlbnRpdHktc2FuZGJveC52d2dyb3VwLmlvIiwianR0IjoiYWNjZXNzX3Rva2VuIiwiZXhwIjoxNTY0NDkwNjI5LCJpYXQiOjE1NjQ0ODcwMjksImxlZSI6WyJWT0xLU1dBR0VOIl0sImp0aSI6IjJkZTAwOTRhLWZiMzEtNDgwNi05YmNjLTQ1Y2EzYTg0ZmY4MSJ9.lpCqJL8oilA0mHg77zP9BMnFsBWFAJgcutKt9x0nURG82CbkSA0T6gJq7zBOPWy-4k-8hdeNRbTMHMTMCWBwkPNSSP1KgTwe_p7D2Ss7sWDe25DjMrC_5baXeHVi44rjvM8nsmescNEQuZx3MFZOJB9fX8p2dtCKnp7PsQd27BD8UJOOMLQ7rIz98hK-oEPWkOJlEi5xNU5bd6IlIDb8EsPBrvrMaOLDHh7Se59KaPWB-CbGBhA_FJ7iL8fqmwL0C7jd8C4wCZC3VXrPeY0l5rO8PXIwjXxn_Taz2S-fOrtmTpvU2LORNVxwjRayzp_kPaRdBWeLj0rCV3uLtyuiTSpAcatmArkYWoV3umU0gF7tLFPvQ9d_ye6ahxjGMTA22BsOP6_TD0JIbOJ7tOXX1lHyCi6UQ1GCX_drUGXFq0zpamjmlNFK1eaMC0_aBdIwDqTwIVC6Inr02_zl-5MtyyX1ekR6yXxQWdTk6DUJb-5ZUOZ-NGPQkPUYFF8nnoJmLh_k8n8CQScyFZaGMHpDgsTPTMResC6xDH1rod4tCqdKvqA_FuK-wFwv_iXTKbi4IzAfAHL2qI-DyGLDphWlaOGZNx1ofe81IA1i_YnH1RjmshJ1eImgNslp_6KlOphWM3L26xeT0TWWRYxgFW__ih7gsclsjJYoQt3CZ_TN_44");
            Console.WriteLine(accessToken);

            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(
                new
                {
                    dealerId = "DEU45080",
                    commissionId = "VDV176",
                    vin = "WVWZZZAUZJW174293",
                    systemId = "eva",
                    status = "SOLDREJ"
                });

            var response = client.Execute(request).StatusCode;
            int numericStatusCode = (int)response;
            Console.WriteLine(numericStatusCode);

            String previousURL = driver.Url;
            /*
            driver.Navigate().GoToUrl(siteUrl + "state");
            //Thread.Sleep(2000);
            try
            {
                clickOnElement(By.XPath("//span[contains(text(),'Unblock car')]"), 10, "can't click on unblock car");
            }
            catch (WebDriverException e)
            {
                Console.WriteLine(e.Message);
            }
            */

            driver.Navigate().Refresh();
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl(previousURL);
        }
    }
}
