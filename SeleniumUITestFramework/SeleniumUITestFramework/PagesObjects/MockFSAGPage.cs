﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using CapyFramework.Helpers;
using CapyFramework.Helpers.Waiter;
using System.Threading;
using OpenQA.Selenium;

namespace SeleniumUITestFramework.PagesObjects
{
    class MockFSAGPage : MainPageOnlineSales
    {
        public MockFSAGPage(RemoteWebDriver driver) : base(driver)
        {
        }
        /// <summary>
        /// Verify that is FSAG response block displayed 
        /// </summary>
        /// <returns></returns>
        public Boolean IsFsagResponceBlockDisplayed()
        {
            
            return S("//div[@class='os-mock-fsag__card']").Displayed;
        }
        /// <summary>
        /// Click on send btn for send request to FSAG
        /// </summary>
        public void ClickOnSendBtn()
        {
            S("//button[@class='os-button os-button--primary']").clickOnElementWithWait(driver);
        }
    }
}
