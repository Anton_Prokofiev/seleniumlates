﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using CapyFramework.Helpers;
using SeleniumUITestFramework.Controls.LightBoxes;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWPaymentMethodSelectionPage : MainPageOnlineSales
    {
        // Payments buttons
        private By DirectPaymentBtnLocator = By.XPath("//button[@id='payment-method-direct-payment-c2a']");
        private By FinancingPaymentBtnLocator = By.XPath("//button[@id='payment-method-financing-c2a']");

        private By PrivacyPolicyErrorMsgLocator = By.XPath("//div[@class='os-checkbox__error']");
        private By PrivacyPolicyChkBoxLocator = By.XPath("//label[@for='agb']//span[contains(@class,'os-checkbox__icon')]");
        private By EmailChkBoxLocator = By.XPath("//label[@for='email-checkbox']//span[contains(@class,'os-checkbox__icon')]");
        private By EmailInputFieldLocator = By.XPath("//input[@id='Input--email']");


        public PKWPaymentMethodSelectionPage(RemoteWebDriver driver) : base(driver)
        {
        }


        public void ClickOnMailChkBox()
        {
            S(EmailChkBoxLocator).clickOnElementWithWait(driver);
        }

        public void ClickOnPrivacyPolicyChkBox()
        {
            S(PrivacyPolicyChkBoxLocator).clickOnElement();
        }
        public void ClickOnDirectPaymentBtn()
        {
            S(DirectPaymentBtnLocator).clickOnElement();
        }
        /// <summary>
        /// Click on Financing button for FSAG process
        /// </summary>
        public LightBox ClickOnFinancingPaymentBtn()
        {
            S(FinancingPaymentBtnLocator).clickOnElementWithWait(driver);
            return driver.CreateLightBox<LightBox>();
        }
        
        public void IsPrivacyPolicyErrMsgIsDisplayed()
        {
            S(PrivacyPolicyErrorMsgLocator);
        }
        public void IsEmailInputFieldEqualVWID()
        {
            S("//div[@class='os-input']").clickOnElementWithWaitJS(driver);
            String email = S(EmailInputFieldLocator).Text;

            Boolean isEmailEqual = email.Equals(ConfigurationManager.AppSettings["Login"]);
            Assert.IsTrue(isEmailEqual, "emails are not equal");
        }
    }
}
