﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using CapyFramework.Helpers;


namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWPaymentPage : MainPageOnlineSales
    {
        public PKWPaymentPage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void ChoosePaymentMethodByBankTransfer()
        {
            S("//button[@id='payment-type-transfer-c2a']").clickOnElement("Can't click on payment method");
            S("//label[@for='agb']//span[contains(@class,'os-checkbox__icon')]").clickOnElement("Can't click on privacy checkbox");
            S("//button[@id='purchase-c2a']").clickOnElement("Can't click on purchase button");
        }
    }
}
