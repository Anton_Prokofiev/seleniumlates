﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using CapyFramework.Helpers;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWAdditionalServiceSummaryPage : MainPageOnlineSales
    {
        public PKWAdditionalServiceSummaryPage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void ClickOnAuslieferungDeliveryMethod()
        {
            S("//button[@id='service-selection-delivery-c2a']").clickOnElementWithWait(driver);
        }
        public void ClickOnAbholungDeliveryMethod()
        {
            S("//button[@id='service-selection-pick-up-c2a']").clickOnElementWithWait(driver);
        }

        public void ClickOnPickUpMethod()
        {
            S("//button[@id='service-selection-pick-up-c2a']").clickOnElementWithWait(driver);
        }

        public void FillLicensePlateFields()
        {
            S("//input[@id='districtCode']").clearAndFill("AQA");
            S("//input[@id='letters']").clearAndFill("QA");
            S("//input[@id='numbers']").clearAndFill("0000");

        }
            public void ChooseTheDateFromCelendarAbholung()
        {
            S("//input[@id='Input--pickupDateOne']").clickOnElementWithWait(driver);

            String datePickerLocator = "(//div[contains(@class,'react-datepicker__day react-datepicker__day') and not(contains(@class,'disabled'))])";

            int upperBound = getElementsCount(By.XPath(datePickerLocator), "can't get count of days in celendar");
            int rnd = randomNumber(1, upperBound);
            S(datePickerLocator + "[" + rnd + "]").clickOnElementWithWait(driver);

        }
        public void ChooseTheDateFromCelendar()
        {
            S("//input[@id='Input--deliveryDateOne']").clickOnElementWithWait(driver);

            String datePickerLocator = "(//div[contains(@class,'react-datepicker__day react-datepicker__day') and not(contains(@class,'disabled'))])";

            int upperBound = getElementsCount(By.XPath(datePickerLocator), "can't get count of days in celendar");
            int rnd = randomNumber(1, upperBound);
            S(datePickerLocator + "[" + rnd + "]").clickOnElementWithWait(driver);
            
        }
        public void ChooseDeliveryTime()
        {
            S("//input[@id='Input--pickupTimeOne']/parent::div").clickOnElementWithWait(driver);
            S("//div[@id='pickupTimeOne-picker']//li[1]").clickOnElementWithWait(driver);           
        }
        public void ClickOndeliveryDateOneMorningChkbox()
        {
            S("//label[@for='deliveryDateOneMorning']").clickOnElementWithWait(driver);
        }
        public LightBox OpenBillindAddressModal()
        {
            S("//button[@class='os-address-summary__button']").clickOnElementWithWait(driver);
            return driver.CreateLightBox<LightBox>();
        }
        public void ClickOnSubmitBtn()
        {
            S("//button[@id='additional-services-summary-next-c2a']").clickOnElementWithWait(driver);
        }
        
    }
}
