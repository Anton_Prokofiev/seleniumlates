﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects
{
    class PKWVolkswagenLoginPage : MainPageOnlineSales
    {
        private Lazy<IWebElement> LoginTextField;
        private Lazy<IWebElement> PwdTextField;
        private Lazy<IWebElement> LoginBtn;

        public PKWVolkswagenLoginPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            LoginTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#email"));
            PwdTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#password"));
            LoginBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#submit-button"));
        }
        public void Open(RemoteWebDriver driver)
        {
        }

        public void Login()
        {
            LoginTextField.Value.SendKeys(ConfigurationManager.AppSettings["Login"]);
            PwdTextField.Value.SendKeys(ConfigurationManager.AppSettings["Pwd"]);
            LoginBtn.Value.Click();
        }
    }
}
