﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using CapyFramework.Helpers;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWOrderSuccessPage : MainPageOnlineSales
    {
        By OrderSuccessPageSectionLocator = By.XPath("//section[contains(@class,'os-section--confirmation')]");
        public bool IsOrderSuccessPageSectionDisplayed => S(OrderSuccessPageSectionLocator).Displayed;
        public PKWOrderSuccessPage(RemoteWebDriver driver) : base(driver)
        {
        }   
    }
}
