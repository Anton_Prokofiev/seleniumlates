﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using CapyFramework.Helpers;
using CapyFramework.Helpers.Waiter;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWHomePage : MainPageOnlineSales
    {
        private static readonly String dealerSectionXPath = "//section[@id='dealer-info']";
        private static readonly String dealerInfoRowXPath = "//span[contains(@class,'os-dealer-info__row')]";
        // Dealer Info section locators
        public By DealerInfoTitle = By.XPath(dealerSectionXPath + "//*[contains(@class,'os-dealer-info__title')]");
        public By DealerInfoAdress = By.XPath(dealerSectionXPath + "//*[contains(@class,'os-dealer-info__copy')]");
        public By DealerInfoTelephone = By.XPath("(" + dealerSectionXPath + dealerInfoRowXPath + ")[3]");
        public By DealerInfoEmail = By.XPath("(" + dealerSectionXPath + dealerInfoRowXPath + ")[4]");
        public By DealerInfoFaqUrl = By.XPath(dealerSectionXPath + "//*[contains(@class,'os-dealer-info__copy')]//a[@target='_blank']");
        // Dealer Info section-footer
        public By DealerInfoFooter = By.XPath(dealerSectionXPath + "//div[contains(@class,'items os-dealer-info__footer')]");



        public Boolean IsDealerInfoTitleDisplayed => S(DealerInfoTitle).Displayed;
        public Boolean IsDealerInfoAdressDisplayed => S(DealerInfoAdress).Displayed;
        public Boolean IsDealerInfoTelephoneDisplayed => S(DealerInfoTelephone).Displayed;
        public Boolean IsDealerInfoEmailDisplayed => S(DealerInfoEmail).Displayed;
        public Boolean IsDealerInfoFooterDisplayed => S(DealerInfoFooter).Displayed;


        public PKWHomePage(RemoteWebDriver driver) : base(driver)
        {
        }
        public LightBox OrderOnlineNowBtnClick()
        {
            S("//button[@id='landing-page-next-c2a']").clickOnElement();
            return driver.CreateLightBox<LightBox>();
        }
        public bool VerifyThatDealerInfoSectionIsDisplayed()
        {
            return S(dealerSectionXPath).Displayed;
        }
        public void ClickOnFaqUrlInDealerInfoSection()
        {
            S(DealerInfoFaqUrl).clickOnElement();
        }
        public bool VerifyThatDealerFooterLinkIsDisplayedById(int Index)
        {
            String DealerInfoFooterLinksXPath = "//div[contains(@class,'items os-dealer-info__footer')]//a[@target='_blank']";
            return S("(" + DealerInfoFooterLinksXPath + ")[" + Index + "]").Displayed;
        }
        public bool VerifyThatDealerFooterLinkIsValidById(int Index)
        {
            String DealerInfoFooterLinksXPath = "//div[contains(@class,'items os-dealer-info__footer')]//a[@target='_blank']";
            String url = S("(" + DealerInfoFooterLinksXPath + ")[" + Index + "]").GetAttribute("href");
            int ResponseCode = Api.GetStatusCodeResponse(url);
            return ResponseCode == 200;
        }
        public bool VerifyThatPrivacyLightBoxLiksIsValidById(int index)
        {
            String url = S("//span[@class='os-checkbox__label']//a[" + index + "]").GetAttribute("href");
            int ResponseCode = Api.GetStatusCodeResponse(url);
            return ResponseCode == 200;
        }
    }
}

